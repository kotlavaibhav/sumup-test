from django.urls import path
from . import views
from . import importdata

app_name = 'quandl'
urlpatterns = [

    path('stock',views.stock, name='stock' ),
    path('quandldata',views.quandldata ),
    path('plotdata',views.plotdata ),
    path('importquandl', importdata.importquandl),
    path('importindicator', importdata.importindicator),
    path('importcountries', importdata.importcountries),
    path('remove',importdata.removeobj),
    
]